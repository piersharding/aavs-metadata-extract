# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c

LOGGING_LEVEL ?= INFO
# FLAGS ?= --parseandexit

BASE = $(shell pwd)

.DEFAULT_GOAL := help

.PHONY: requirements run test lint clean build help jupyter

# define private rules and additional makefiles
-include PrivateRules.mak

vars:
	@echo GITLAB_API_REQUESTER=$(GITLAB_API_REQUESTER)
	@echo GITLAB_API_PRIVATE_TOKEN=$(GITLAB_API_PRIVATE_TOKEN)
	@echo RTD_TOKEN=$(RTD_TOKEN)

clean:  ## Clean build
	mkdir -p dist
	rm -rf dist/*

requirements:  ## install the requirements for project
	# install node >=12 first https://computingforgeeks.com/how-to-install-nodejs-on-ubuntu-debian-linux-mint/
	poetry install --only main

dev-requirements:  ## install the requirements for project
	# install node >=12 first https://computingforgeeks.com/how-to-install-nodejs-on-ubuntu-debian-linux-mint/
	poetry install

shell:  ## Run poetry shell
	PYTHONPATH=./src GOOGLE_APPLICATION_CREDENTIALS=$(GAC) \
	GITLAB_TOKEN=$(GITLAB_TOKEN) RTD_TOKEN=$(RTD_TOKEN) \
	LOGGING_LEVEL=$(LOGGING_LEVEL) poetry shell

lint:
	black voltage-files.py all-files.py
	isort voltage-files.py all-files.py

DIR ?= ./data
CSV ?= # add --csv
run:  ## Run the scraper
	@echo "Start: $$(date)"
	time \
	LOGGING_LEVEL=$(LOGGING_LEVEL) poetry run python voltage-files.py \
	--recurse $(CSV) --vdir $(DIR) --vprefix=$(DIR) > metadata.csv 2>log.out
	@echo "Finish: $$(date)"

test: LOGGING_LEVEL:=DEBUG
test:  ## Run the scraper
	@echo "Start: $$(date)"
	time \
	LOGGING_LEVEL=$(LOGGING_LEVEL) poetry run python voltage-files.py \
	--recurse --vdir $(DIR) --vprefix=$(DIR) --csv
	@echo "Finish: $$(date)"

FILE ?=
runf:  ## Run the scraper
	LOGGING_LEVEL=$(LOGGING_LEVEL) poetry run python voltage-files.py --pprint --recurse --vfile $(FILE)


stats:
	gunzip -c all-files.txt.gz | ./all-files.py  > files.csv

jupyter: ## Jupyter notebook for exploring commit data
	poetry run jupyter-lab ./aavs-data.ipynb

