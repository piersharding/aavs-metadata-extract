#!/usr/bin/env python3
import gzip
import json
import logging
import os
import pathlib
import string
import sys

import click
import h5py
import yaml

META_BLOCK = 1024

CSV_HEADERS = [
    "observation",
    "description",
    "firmware_version",
    "software_version",
    "start_frequency_channel",
    "sample_timestamps",
    "bandwidth",
    "channel_id",
    "data_mode",
    "data_type",
    "date_time",
    "n_antennas",
    "n_baselines",
    "n_beams",
    "n_blocks",
    "n_chans",
    "n_pols",
    "n_samples",
    "n_stokes",
    "station_id",
    "tile_id",
    "timestamp",
    "ts_end",
    "ts_start",
    "tsamp",
    "type",
    "file",
    "file_type",
    "file_size",
    "file_size_raw",
    "HDR_VERSION",
    "HDR_SIZE",
    "BW",
    "FREQ",
    "TELESCOPE",
    "RECEIVER",
    "INSTRUMENT",
    "SOURCE",
    "MODE",
    "NBIT",
    "NPOL",
    "NCHAN",
    "NDIM",
    "OBS_OFFSET",
    "TSAMP",
    "UTC_START",
    "POPULATED",
    "OBS_ID",
    "SUBOBS_ID",
    "COMMAND",
    "NTIMESAMPLES",
    "NINPUTS",
    "NINPUTS_XGPU",
    "METADATA_BEAMS",
    "APPLY_PATH_WEIGHTS",
    "APPLY_PATH_DELAYS",
    "INT_TIME_MSEC",
    "FSCRUNCH_FACTOR",
    "TRANSFER_SIZE",
    "PROJ_ID",
    "EXPOSURE_SECS",
    "COARSE_CHANNEL",
    "CORR_COARSE_CHANNEL",
    "SECS_PER_SUBOBS",
    "UNIXTIME",
    "UNIXTIME_MSEC",
    "FINE_CHAN_WIDTH_HZ",
    "NFINE_CHAN",
    "BANDWIDTH_HZ",
    "SAMPLE_RATE",
    "MC_IP",
    "MC_SRC_IP",
    "FILE_SIZE",
    "FILE_NUMBER",
]
VPREFIX = ""


LOGGING_LEVEL = os.environ.get("LOGGING_LEVEL", "INFO").upper()
LOGGING_FORMAT = (
    "%(asctime)s [level=%(levelname)s] [thread=%(threadName)s] "
    "[module=%(module)s] [line=%(lineno)d]: %(message)s"
)
logging.basicConfig(level=LOGGING_LEVEL, format=LOGGING_FORMAT)
log = logging.getLogger("voltage-files")


@click.command()
@click.option("--vfile", default="", help="An HDF5/DADA file to scan", required=False)
@click.option("--vdir", default="", help="An HDF5/DADA dir to scan", required=False)
@click.option(
    "--vprefix",
    default="./data",
    help="Dir prefix to the observation list",
    required=False,
)
@click.option("--recurse/--no-recurse", default=False)
@click.option("--csv/--no-csv", default=False)
@click.option("--pprint/--no-pprint", default=False)
@click.option("--debug/--no-debug", default=False)
def voltage_file(vfile, vdir, vprefix, recurse, csv, pprint, debug):
    """Scan HDF5 voltage files."""
    if debug:
        log.setLevel("DEBUG")

    # reset the prefix globally based on switches
    global VPREFIX
    VPREFIX = os.path.abspath(vprefix)
    if not VPREFIX.endswith("/"):
        VPREFIX += "/"

    # output CSV
    if csv:
        print("\t".join(CSV_HEADERS))
    csv_data = []

    mdata = {}

    # what type of path is provided?
    if os.path.isdir(vdir):
        if recurse:
            log.info(
                "Reading HDF5 voltage files from multiple dirs (recursive): {vdir}".format(
                    vdir=vdir
                )
            )
            d = pathlib.Path(os.path.abspath(vdir))
            for f in d.rglob("*.*"):
                # only support hdf5 and dada files
                f = str(f)
                if f.endswith(".hdf5") or f.endswith(".hdf5.gz"):
                    log.debug("file: {f}".format(f=f))
                    mdata = extract_metadata_hdf5(f)
                    if not mdata:
                        log.error(
                            "Failed to read HDF5 file (rglob empty): {0}".format(f)
                        )
                    else:
                        output_csv(csv, mdata, f)
                elif f.endswith(".dada") or f.endswith(".dada.gz"):
                    log.debug("file: {f}".format(f=f))
                    mdata = extract_metadata_dada(f)
                    if not mdata:
                        log.error(
                            "Failed to read DADA file (rglob empty): {0}".format(f)
                        )
                    else:
                        output_csv(csv, mdata, f)

        else:
            log.info(
                "Reading HDF5 voltage files from single dir: {vdir}".format(vdir=vdir)
            )
            d = pathlib.Path(os.path.abspath(vdir))
            for f in d.glob("*.hdf5*"):
                log.debug("file: {f}".format(f=f))
                f = str(f)
                if f.endswith(".hdf5") or f.endswith(".hdf5.gz"):
                    log.debug("file: {f}".format(f=f))
                    mdata = extract_metadata_hdf5(f)
                    if not mdata:
                        log.error(
                            "Failed to read HDF5 file (glob empty): {0}".format(f)
                        )
                    output_csv(csv, mdata, f)

    elif os.path.isfile(vfile):
        log.info(
            "Reading HDF5/DADA voltage files from file: {vfile}".format(vfile=vfile)
        )
        # "./data/2023_05_24_ch204_corr_DEMO/voltages/channel_cont_0_20230524_32817_0.hdf5"
        if vfile.endswith(".hdf5") or vfile.endswith(".hdf5.gz"):
            mdata = extract_metadata_hdf5(vfile)
        elif vfile.endswith(".dada"):
            mdata = extract_metadata_dada(vfile)
        if not mdata:
            log.error("Failed to read HDF5 file (empty): {0}".format(vfile))
        if pprint:
            pprint_json(mdata)
    else:
        raise (click.ClickException("Must provide a valid --vfile or --vdir"))


def open_dada(vfile):
    """Open DADA with basic sanity checking"""

    # check file/link is really there
    if os.path.islink(vfile):
        if not os.path.exists(os.readlink(vfile)):
            log.error("Voltage file symlink is unreadable: {vfile}".format(vfile=vfile))
            return None

    fh = None
    try:
        if vfile.endswith(".gz"):
            fh = gzip.open(vfile)
        else:
            fh = open(vfile, errors="ignore")

        return fh
    except Exception as e:
        log.error("cannot ope: {vfile} -  {e} skipping".format(vfile=vfile, e=str(e)))
        raise e


def extract_strings(filename, min=4):
    """Get stings from a binary file"""
    cnt = 0
    with open_dada(filename) as f:  # Python 3.x
        result = ""
        for c in f.read(META_BLOCK):
            cnt += len(c)
            # log.debug("cnt: {}".format(cnt))
            if c == "\x00":
                # log.debug("Found null")
                break
            # if cnt > META_BLOCK:
            #     break
            if c in string.printable:
                result += c
                continue
            if len(result) >= min:
                yield result
            result = ""
        if len(result) >= min:  # catch result at EOF
            yield result


def output_csv(csv, data, file):
    """output CSV"""
    if csv:
        if file.startswith(VPREFIX):
            data["observation"] = file[len(VPREFIX) :].split("/")[0]
        else:
            data["observation"] = file.split("/")[0]
        data["file"] = file.split("/")[-1]
        try:
            file_stats = os.stat(file)
            data["file_size"] = str(int(file_stats.st_size / (1024 * 1024))) + "MB"
            data["file_size_raw"] = str(int(file_stats.st_size))
        except FileNotFoundError as e:
            data["file_size"] = "NotFound"
        print("\t".join([str(data[k] if k in data else None) for k in CSV_HEADERS]))
        sys.stdout.flush()


def pprint_json(data):
    """Pretty Print JSON"""
    print(json.dumps(data, indent=2))


def open_hdf5(vfile):
    """Open HDF5 with basic sanity checking"""

    # check file/link is really there
    if os.path.islink(vfile):
        if not os.path.exists(os.readlink(vfile)):
            log.error("Voltage file symlink is unreadable: {vfile}".format(vfile=vfile))
            return None

    fh = None
    if vfile.endswith(".gz"):
        fh = gzip.open(vfile, "rb")
    else:
        fh = open(vfile, "rb")
    try:
        f = h5py.File(fh, "r")
    except Exception as exc:
        # handle HDF5 format errors
        # print(exc)
        log.error(
            "Voltage file is unreadable: {vfile} - exception: {exc}".format(
                vfile=vfile, exc=str(exc)
            )
        )
        return None
        # raise(click.ClickException("Voltage file is unreadable: {vfile}".format(vfile=vfile)))
    except OSError as exc:
        # handle os related errors
        raise (
            click.ClickException(
                "Voltage file unknown error: {vfile} / {err}".format(
                    vfile=vfile, err=str(exc)
                )
            )
        )
    return f


def extract_metadata_dada(vfile):
    """Extract the metadata from an DADA file"""
    try:
        mdata = {"file_type": "dada"}
        s = [s for s in extract_strings(vfile)].pop()
        mdada = {f[0]: f[1] for f in [l.split() for l in s.splitlines()]}
        log.debug(mdada)
        return {**mdata, **mdada}
    except:
        log.warn("skipping file")
        return None


def extract_metadata_hdf5(vfile):
    """Extract the metadata from an HDF5 file"""
    mdata = {"file_type": "hdf5"}
    f = open_hdf5(vfile)
    # bail if something went wrong with the file
    if not f:
        return mdata

    # print("%.8f\n",f['sample_timestamps']['data'][0][0])
    if (
        not "sample_timestamps" in f
        or not "data" in f["sample_timestamps"]
        # IndexError
    ):
        log.error("sample_timestamps missing: {vfile} - skipping".format(vfile=vfile))
        mdata["sample_timestamps"] = None
    else:
        try:
            mdata["sample_timestamps"] = "{:.8f}".format(
                f["sample_timestamps"]["data"][0][0]
            )
        except Exception as e:
            log.error(
                "sample_timestamps empty in: {vfile} -  {e} skipping".format(
                    vfile=vfile, e=str(e)
                )
            )
            mdata["sample_timestamps"] = None

    # grab the metadata from obersation info
    oi_keys = ["description", "firmware_version", "software_version"]
    if not "observation_info" in f:
        log.error("observation_info missing in: {vfile} - skipping".format(vfile=vfile))
    else:
        mdata = {
            k: (
                str(
                    f["observation_info"].attrs[k]
                    if k in f["observation_info"].attrs
                    else None
                )
            )
            for k in oi_keys
        }

        # unpack yaml station config and add to metadata
        if not "station_config" in f["observation_info"].attrs:
            log.error(
                "station_config missing in observation_info of: {vfile} - skipping".format(
                    vfile=vfile
                )
            )
        else:
            try:
                y = yaml.safe_load(f["observation_info"].attrs["station_config"])
                for k in y["observation"].keys():
                    mdata[k] = y["observation"][k]
            except yaml.YAMLError as exc:
                log.error(
                    "Failed to unpack station_config: {vfile} - exception: {exc}".format(
                        vfile=vfile, exc=str(exc)
                    )
                )
            except Exception as exc:
                log.error(
                    "Failed to find station_config: {vfile} - exception: {exc}".format(
                        vfile=vfile, exc=str(exc)
                    )
                )

    # add root attrs last - may overwrite?
    if not "root" in f:
        log.error(
            "Failed to find root attrs in: {vfile} - exception: {exc}".format(
                vfile=vfile, exc=str(exc)
            )
        )
    else:
        for k in f["root"].attrs.keys():
            mdata[k] = str(f["root"].attrs[k])

    return mdata


if __name__ == "__main__":
    voltage_file()


### Handle TAI
# from datetime import datetime, timedelta
# Y2K_EPOCH = datetime(2000, 1, 1) -  timedelta(seconds=32)
# from grain import Grain
# g = Grain()
# t1 = datetime.fromtimestamp(1684919515.486469)
# g.utc2tai(t1, epoch=Y2K_EPOCH)
