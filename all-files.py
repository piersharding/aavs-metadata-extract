#!/usr/bin/env python3
import logging
import os
import sys

LOGGING_LEVEL = os.environ.get("LOGGING_LEVEL", "INFO").upper()
LOGGING_FORMAT = (
    "%(asctime)s [level=%(levelname)s] [thread=%(threadName)s] "
    "[module=%(module)s] [line=%(lineno)d]: %(message)s"
)
logging.basicConfig(level=LOGGING_LEVEL, format=LOGGING_FORMAT)
log = logging.getLogger("all-files")


def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix) :]
    return text


obs = {}
for line in sys.stdin:
    l = remove_prefix(line.rstrip(), "./").split("/")
    # if not l[0] == "2023_06_17_pulsars_vela":
    #     continue
    # if not l[-1].endswith('.hdf5'):
    #     continue

    # print(l)
    if not l[0] in obs:
        obs[l[0]] = {
            "hdf5": 0,
            "dat": 0,
            "dada": 0,
            "xxyy": 0,
            "dirs": {},
            "voltages": 0,
            "merged": 0,
            "other": 0,
        }
    if (
        l[-1].endswith(".hdf5")
        or l[-1].endswith(".hdf5_info")
        or l[-1].endswith(".hdf5.gz")
        or l[-1].endswith(".dada")
        or l[-1].endswith(".dat")
        or l[-1] in ["header", "history", "image"]
    ):
        # log.info(l[-2])
        if (
            l[-1].endswith(".hdf5")
            or l[-1].endswith(".hdf5_info")
            or l[-1].endswith(".hdf5.gz")
        ):
            obs[l[0]]["hdf5"] += 1
        if l[-1].endswith(".dada"):
            obs[l[0]]["dada"] += 1
        if l[-1].endswith(".dat"):
            obs[l[0]]["dat"] += 1
        if l[-1] in ["header", "history", "image"]:
            obs[l[0]]["xxyy"] += 1
        # don't report the XX YY dirs as there are gazillions of them
        if not l[-2] in obs[l[0]]["dirs"] and not l[-1] in [
            "header",
            "history",
            "image",
        ]:
            obs[l[0]]["dirs"][l[-2]] = 1
        if l[-2] == "voltages":
            obs[l[0]]["voltages"] += 1
        elif l[-2] == "merged":
            obs[l[0]]["merged"] += 1
        else:
            obs[l[0]]["other"] += 1

k = ["hdf5", "dada", "dat", "xxyy", "voltages", "merged", "other", "dirs"]
print("\t".join(["obs"] + k))

for o in obs.keys():
    obs[o]["dirs"] = '"' + ":".join(obs[o]["dirs"].keys()) + '"'
    # log.info("{0} => {1}".format(o, obs[o]['dirs']))
    print("\t".join([o] + [str(obs[o][v]) for v in k]))

# print(obs)
